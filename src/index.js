const {
    O2Measurement,
    PulseMeasurement,
    TemperatureMeasurement,
    BpMeasurement
} = require('./models/Measurement');
const Device = require('./models/Device');
const { createDevice, createMeasurement, createAlarm, createEvent } = require('./api/api')
const Alarm = require('./models/Alarm');
const Event = require('./models/Event');

const auth = {
    username: process.env.USERNAME,
    password: process.env.PASSWORD
}

const device1 = new Device("Smartwatch-120")
const device2 = new Device("Smartwatch-122")
const device3 = new Device("Smartwatch-123")


createDevice(device1, auth)
    .then(async ({ data: { id, name } }) => {
        try {
            console.log(name, id)

            const o2 = new O2Measurement(new Date(), { id }, 77, '%')
            const pulse = new PulseMeasurement(new Date(), { id }, 72, '/min')
            const bp = new BpMeasurement(new Date(), { id }, 85, 120, "mmHg")
            const temp = new TemperatureMeasurement(new Date(), { id }, 18, 'C')

            await createMeasurement(o2, auth)
            await createMeasurement(pulse, auth)
            await createMeasurement(bp, auth)
            await createMeasurement(temp, auth)

            const walkingEvent = new Event(new Date(), { id }, "walk", "User started walking")
            const runningEvent = new Event(new Date(), { id }, "run", "User started running")

            await createEvent(walkingEvent, auth)
            await createEvent(runningEvent, auth)

            const pulseAlarm = new Alarm(new Date(), { id }, 'pulseAlarm', 'User has high heart rate.', "MAJOR", "ACTIVE")
            const bpAlarm = new Alarm(new Date(), { id }, 'bpAlarm', 'User has high blood pressure.', "CRITICAL", "ACTIVE")

            await createAlarm(pulseAlarm, auth)
            await createAlarm(bpAlarm, auth)

            console.log("Events and alarms set for" + name)
        } catch (e) {
            console.log(e.message)
        }
    })
    .catch(e => console.log("Device creation failed: " + e.message))

createDevice(device2, auth)
    .then(async ({ data: { id, name } }) => {
        try {
            console.log(name, id)

            const o2 = new O2Measurement(new Date(), { id }, 85, '%')
            const pulse = new PulseMeasurement(new Date(), { id }, 88, '/min')
            const bp = new BpMeasurement(new Date(), { id }, 85, 125, "mmHg")
            const temp = new TemperatureMeasurement(new Date(), { id }, 30, 'C')

            await createMeasurement(o2, auth)
            await createMeasurement(pulse, auth)
            await createMeasurement(bp, auth)
            await createMeasurement(temp, auth)

            const walkingEvent = new Event(new Date(), { id }, "walk", "User started walking")
            const runningEvent = new Event(new Date(), { id }, "run", "User started running")

            await createEvent(walkingEvent, auth)
            await createEvent(runningEvent, auth)

            const pulseAlarm = new Alarm(new Date(), { id }, 'pulseAlarm', 'User has high heart rate.', "MAJOR", "ACTIVE")
            const bpAlarm = new Alarm(new Date(), { id }, 'bpAlarm', 'User has high blood pressure.', "CRITICAL", "ACTIVE")

            await createAlarm(pulseAlarm, auth)
            await createAlarm(bpAlarm, auth)

            console.log("Events and alarms set for" + name)
        } catch (e) {
            console.log(e.message)
        }
    })
    .catch(e => console.log("Device creation failed: " + e.message))

createDevice(device3, auth)
    .then(async ({ data: { id, name } }) => {
        try {
            console.log(name, id)

            const o2 = new O2Measurement(new Date(), { id }, 77, '%')
            const pulse = new PulseMeasurement(new Date(), { id }, 72, '/min')
            const bp = new BpMeasurement(new Date(), { id }, 85, 120, "mmHg")
            const temp = new TemperatureMeasurement(new Date(), { id }, 18, 'C')

            await createMeasurement(o2, auth)
            await createMeasurement(pulse, auth)
            await createMeasurement(bp, auth)
            await createMeasurement(temp, auth)

            const walkingEvent = new Event(new Date(), { id }, "walk", "User started walking")
            const runningEvent = new Event(new Date(), { id }, "run", "User started running")

            await createEvent(walkingEvent, auth)
            await createEvent(runningEvent, auth)

            const pulseAlarm = new Alarm(new Date(), { id }, 'pulseAlarm', 'User has high heart rate.', "MAJOR", "ACTIVE")
            const bpAlarm = new Alarm(new Date(), { id }, 'bpAlarm', 'User has high blood pressure.', "CRITICAL", "ACTIVE")

            await createAlarm(pulseAlarm, auth)
            await createAlarm(bpAlarm, auth)

            console.log("Events and alarms set for" + name)
        } catch (e) {
            console.log(e.message)
        }
    })
    .catch(e => console.log("Device creation failed: " + e.message))