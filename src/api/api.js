const axios = require('axios');

const createDevice = async (device, auth) => {
    const res = await axios.post(
        `${process.env.URL}inventory/managedObjects`,
        device,
        {
            auth,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
    return res
}

const createMeasurement = async (measurement, auth) => {
    const res = await axios.post(
        `${process.env.URL}measurement/measurements`,
        measurement,
        {
            auth,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
    return res
}

const createAlarm = async (alarm, auth) => {
    const res = await axios.post(
        `${process.env.URL}alarm/alarms`,
        alarm,
        {
            auth,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
    return res
}

const createEvent = async (event, auth) => {
    const res = await axios.post(
        `${process.env.URL}event/events`,
        event,
        {
            auth,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
    return res
}

const getDevice = async (id, auth) => {
    const res = await axios.get(
        `${process.env.URL}inventory/managedObjects/${id}`,
        {
            auth,
            headers: {
                'Content-Type': 'application/vnd.com.nsn.cumulocity.event+json',
                'Accept': 'application/vnd.com.nsn.cumulocity.event+json'
            }
        }
    )
    return res
}

module.exports = { createDevice, getDevice, createMeasurement, createAlarm, createEvent }