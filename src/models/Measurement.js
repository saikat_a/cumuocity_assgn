class BaseMeasurement {
    constructor(time, source, type) {
        this.time = time
        this.source = source
        this.type = type
    }
}

class PulseMeasurement extends BaseMeasurement {
    constructor(time, source, value, unit) {
        super(time, source, "pulse_measurement")
        this.com_cumulocity_model_smartwatch_pulse_measurement = {
            Pulse: { value, unit }
        }
    }
}

class BpMeasurement extends BaseMeasurement {
    constructor(time, source, sys, dia, unit) {
        super(time, source, "bp_measurement")
        this.com_cumulocity_model_smartwatch_bp_measurement = {
            Sys: { value: sys, unit },
            Dia: { value: dia, unit }
        }
    }
}

class O2Measurement extends BaseMeasurement {
    constructor(time, source, value, unit) {
        super(time, source, "o2_measurement")
        this.com_cumulocity_model_smartwatch_o2_measurement = {
            O2: { value, unit }
        }
    }
}

class TemperatureMeasurement extends BaseMeasurement {
    constructor(time, source, value, unit) {
        super(time, source, "c8y_TemperatureMeasurement")
        this.c8y_TemperatureMeasurement = {
            T: { value, unit }
        }
    }
}

module.exports = {
    BaseMeasurement,
    O2Measurement,
    TemperatureMeasurement,
    PulseMeasurement,
    BpMeasurement
}
