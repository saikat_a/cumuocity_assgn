class Alarm {
    constructor(time, source, type, text, severity, status) {
        this.time = time
        this.source = source
        this.type = type
        this.text = text
        this.severity = severity
        this.status = status
    }
}

module.exports = Alarm