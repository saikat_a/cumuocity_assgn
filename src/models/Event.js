class Event {
    constructor(time, source, type, text) {
        this.time = time
        this.source = source
        this.type = type
        this.text = text
    }
}

module.exports = Event